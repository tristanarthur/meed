```
███╗   ███╗███████╗███████╗██████╗ 
████╗ ████║██╔════╝██╔════╝██╔══██╗
██╔████╔██║█████╗  █████╗  ██║  ██║
██║╚██╔╝██║██╔══╝  ██╔══╝  ██║  ██║
██║ ╚═╝ ██║███████╗███████╗██████╔╝
╚═╝     ╚═╝╚══════╝╚══════╝╚═════╝   
```
`meed` is a configurable stats logger that can be used in the command line and as a GUI. It was originally designed to
periodically query for the user's mood scores but can be used to track any stats.

## Installation
### Install Tkinter
See https://www.tutorialspoint.com/how-to-install-tkinter-for-python-on-linux for instructions.

### Using pip
```
pip3 install meed
```
### Manually
Pull the source code from the repository and ensure you have pybuilder installed
```
pip3 install pybuilder
```
In the project root, build the package
```
pyb install
```

## Usage
```
meed -h
```

### Create a new profile
To start using `meed` you will need to configure a new profile.
```
meed --new
```
**REQUIRED**
* `Profile_Name` is a unique identifier for your profile.
* `Output_File` in the file path that `meed` will output all the stats to.

**OPTIONAL**
* `AWS_Profile` has not been implemented yet. Eventually meed will use this to backup stats to your AWS account.
* `Theme` `meed`'s GUI is built on-top of PySimpleGUI, see [here](https://www.geeksforgeeks.org/themes-in-pysimplegui/) for a list of themes.
* `Stats` is a comma seperated list of stats you would like to log.
* `Depth` for each stat meed will provide an option between 1 and `Depth`.
* `Style` `Radio` is the default meed style, `Button` and `Slider` are still under development.

The configuration file

### Test the new Profile
```
meed log <Profile_Name>
```
The `meed` log window will appear and will look something similar too:

![](docs/resources/meed_log.png)

From here you can either submit your first mood check or cancel. `meed` will save all stats in the `Output_File` file
path.

### Re-ocurring Meed Log
`meed` makes use of cronjobs to perform re-occurring actions. See [here](https://www.tutorialspoint.com/unix_commands/crontab.htm) 
for a `crontab` guide. For basic example on how to setup a cronjob follow:
```
crontab -e
```
On a new line enter:
```
0 16 * * 1-5  DISPLAY=<:N> <full_path_to_meed> log <Profile_Name>
```
Replace `<:N>` with the output from `echo $DISPLAY` Replace `<full_path_to_meed`> with the path to the `meed` script. This can generally be found with `whereis meed`.
In future cron will be managed within the script to avoid having to manually configure cron.
With this `cron` expression, every weekday at 4pm `meed` will ask to 


## Development
In the Project root
```
pyb && pyb install
```
will build and install `meed`. You can then use the `meed` command in the terminal.