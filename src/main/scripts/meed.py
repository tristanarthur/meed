#!/usr/bin/python
from meedlib import cli
from sys import exit


def main():
    exit(cli())


if __name__ == "__main__":
    main()
