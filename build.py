from pybuilder.core import use_plugin, init, Author

use_plugin("python.core")
use_plugin("python.unittest")
use_plugin("python.flake8")
use_plugin("python.coverage")
use_plugin("python.distutils")
use_plugin("python.install_dependencies")

use_plugin("filter_resources")


name = "meed"
summary = "A configurable stats logger for the end user"
default_task = ["clean", "analyze", "publish"]
authors = (Author("Tristan Arthur", "tristanarthur@pm.me"),)
version = "0.0.3"
license = "GNU General Public License V3"


@init
def set_properties(project):

    # Base
    project.depends_on_requirements("requirements.txt")
    project.build_depends_on("pybuilder>=0.12.10")
    project.build_depends_on("black>=21.8b0")

    # Flake
    project.set_property("flake8_break_build", False)

    # DistUtils
    project.set_property("distutils_author_email", "tristanarthur@pm.me")
    project.set_property("distutils_console_scripts", ["meed=meed:main"])
    project.set_property("distutils_classifiers", [
        'Intended Audience :: Customer Service',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.9',
    ])
    project.set_property("distutils_upload_repository", "https://pypi.org/legacy/")
    project.set_property("distutils_readme_description", True)

    # Tests
    project.set_property("coverage_break_build", False)  # This should be set to True eventually

    # Filter Files
    project.get_property("filter_resources_glob").append("**/meedlib/base.py")
